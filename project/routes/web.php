<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'SetSameSiteCookie'], function () {
    Route::get('/', function () {
        return view('errors.un_verrified_shopify_request');
    });

    Route::name('un_verrified_shopify_request')->get('un_verrified_shopify_request', 'ShopifyController@un_verrified_shopify_request');




    Route::get('main_app_page/', 'ShopifyController@main_app_page')
        ->name('main_app_page');
    Route::name('install')->get('install', 'ShopifyController@install_app');
    Route::get('shopifycallback', 'ShopifyController@shopify_callback')->name('shopifycallback');
    Route::get('download/xml/{id}', 'WebhookReceiverController@xmlDownload')->name('xmlDownload');
    Route::post('upload/xml', 'WebhookReceiverController@uploadXML')->name('xmlUpload');
});
Route::name('badLogin')->get('badLogin', 'ShopifyController@badLogin');


Route::post('process_shopify_webhooks', 'WebhookReceiverController@process_shopify_webhooks')->name('process_shopify_webhooks');
Route::post('product_creation', 'WebhookReceiverController@itemCreate')->name('itemCreate');
Route::post('product_cancellation', 'WebhookReceiverController@orderCancellation')->name('orderCancellation');


Route::group(['middleware' => ''], function () {
//receive orders route for slack notifications
    Route::post('appunistalled_webhook', 'ShopifyController@removeApp')->name('appunistalled_webhook');
    Route::any('customers/redact', 'WebhookReceiverController@customers_redact')->name('customers_redact');
    Route::any('shop/redact', 'WebhookReceiverController@shop_redact')->name('shop_redact');
    Route::any('customers/data_request', 'WebhookReceiverController@customers_data_request')->name('customers_data_request');
});


Route::get('token', 'HooksHandleController@token')->name('token');
Route::post('ajax/update', 'HooksHandleController@CountView')->name('ajax.update');
Route::get('ajax/counter', 'HooksHandleController@CountDisplay')->name('ajax.counter');
Route::get('/locale/{locale}', function ($locale){
    session(['locale'=> $locale]);
    return redirect()->back();
});
Route::get('urlCodeXML', 'XMLHandlerController@urlToXML');
Route::post('settings_counter', 'ShopifyController@Setting');
Route::post('post_detail', 'ShopifyController@post_detail');
Route::get('post/{id}/{title}', 'ShopifyController@post_detail');

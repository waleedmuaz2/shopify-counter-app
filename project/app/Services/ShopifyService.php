<?php

namespace App\Services;

use App\Entities\PlansHistory;
use App\Entities\StoreWebhook;
use Oseintow\Shopify\Facades\Shopify;
use Session;
use URL;
use App\Entities\Shop;

class ShopifyService
{
    private $api_version;

    public function __construct()
    {
        $this->api_version = config('shopify.api_version');
    }



    public function verify_webhooks($shop, $topic = null, $callback_url = null)
    {
// need to update this function with required webhhooks
        $shopUrl = $shop->myshopify_domain;
        $accessToken = $shop->access_token;
        $webhooks = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/api/$this->api_version/webhooks.json", ["limit" => 250]);
//          dd($webhooks);

        $webhook_topic_already_exists = false;
        $webhook_activated = true;
        if ($webhooks && sizeof($webhooks) > 0) {
            $createAppUnistallWebhook = false;
            $str = '';
            foreach ($webhooks as $webhook):
                $str .= $webhook->topic . "\n";
                if ($topic && $callback_url && $webhook->topic == $topic) {
                    $webhook_topic_already_exists = true;
                }
                // use first time on apps installation in store
                if ($webhook->topic == 'app/uninstalled') {
                    $createAppUnistallWebhook = true;
                }

            endforeach;
            // bind webhook to for store on creating new event for any channel
            if ($topic && $callback_url && !$webhook_topic_already_exists) {
                $webhook_activated = $this->create_webhook($shop, $topic, $callback_url);
            }

            // use first time on apps installation in store
            if (!$createAppUnistallWebhook) {
                $is_success = $this->create_webhook($shop, 'app/uninstalled', 'appunistalled_webhook');
                if ($is_success) {
                    $shop->is_webhooks_added = 1;
                    $shop->save();
                }

            }

        } else {
            // bind webhook to for store on creating new event for any channel
            if ($topic && $callback_url) {
                $webhook_activated = $this->create_webhook($shop, $topic, $callback_url);
            }
//            create webhooks forapp removing
            $is_success = $this->create_webhook($shop, 'app/uninstalled', 'appunistalled_webhook');
            $is_success = $this->create_webhook($shop, 'orders/create', 'process_shopify_webhooks');
            $is_success = $this->create_webhook($shop, 'orders/update', 'process_shopify_webhooks');
            $is_success = $this->create_webhook($shop, 'products/create', 'process_shopify_webhooks');
            if ($is_success) {
                $shop->is_webhooks_added = 1;
                $shop->save();
            }
        }

        return $webhook_activated;

    }

    private function create_webhook($shop, $type, $callbacK_url)
    {
        $shopUrl = $shop->myshopify_domain;
        $accessToken = $shop->access_token;
        $callbacK_url = URL::to('/') . '/' . $callbacK_url;
        $post_data = array('webhook' => array('topic' => $type, 'address' => $callbacK_url, 'format' => 'json'));
        try {
            $webhook = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->post("admin/api/$this->api_version/webhooks.json", $post_data);
            return true;
        } catch (\Exception $e) {
            // dd($e);
            return false;

        }
    }

    public function remove_single_webhook($shop, $topic_name)
    {
        $accessToken = $shop['access_token'];
        $shopUrl = $shop['myshopify_domain'];
        $webhooks = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/api/$this->api_version/webhooks.json");
        if ($webhooks && sizeof($webhooks) > 0) {
            foreach ($webhooks as $webhook):
                if ($webhook->topic == $topic_name) {
                    $webhook_id = $webhook->id;
                    try {
                        Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->delete("admin/api/$this->api_version/webhooks/$webhook_id.json");
                    } catch (\Exception $e) {
                        return false;
                    }

                }
            endforeach;
            return true;
        }
        return true;
    }




}

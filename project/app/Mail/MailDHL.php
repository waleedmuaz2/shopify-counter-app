<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailDHL extends Mailable
{
    use Queueable, SerializesModels;
    public $fileName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fileName)
    {
        $this->fileName=$fileName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to('dhl@dhl.com')
            ->subject('You Have New Order ( DHL )')
            ->view('emails.dhl-mail')
            ->attach(url('project/storage/app/public/in').'/'.$this->fileName);
    }
}

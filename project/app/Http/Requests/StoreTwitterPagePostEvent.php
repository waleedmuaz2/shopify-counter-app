<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTwitterPagePostEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'twitter_account_id' => 'required',
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'tweet_message' => 'required',


        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [

                'twitter_account_id.required' => "No twitter account details found",
                'channel_id.required' => "No channel details found.",
                'tweet_message.required' => "Write tweet message to post.",

            ];
        return $error_messages;
    }
}

<?php

namespace App\Http\Requests;

use App\Entities\GoogleContactEvent;
use Illuminate\Foundation\Http\FormRequest;

class StoreGoogleContactEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'google_account_id' => 'required',
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'google_contact_event_id' => 'required',
        ];
        $google_contact_event = GoogleContactEvent::where('id', $this->request->get('google_contact_event_id'))
            ->first();
        if ($google_contact_event->slug != 'upload_contact_photo') {
            $rules['first_name'] = 'required_without_all:middle_name,last_name,job_title,company,email,email_type,phone_number,phone_type,address_street,address_po_box,address_city,address_state,address_zip,address_country,address_type,notes';
            $rules['email'] = 'required_without_all:first_name,phone_number';
            $rules['email_type'] = 'required_with_all:email';
            $rules['phone_number'] = 'required_without_all:first_name,email';
            $rules['phone_type'] = 'required_with_all:phone_number';

        }
        if ($google_contact_event->slug == 'upload_contact_photo') {
            $rules['contact_photo_link'] = 'required';
        }

        if ($google_contact_event->slug != 'create_contact') {
            $rules['google_contact_resource_id'] = 'required';

        }
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [

                'google_contact_event_id.required' => "Please choose google contact event type to continue.",
                'google_account_id.required' => "No google account details found",
                'channel_id.required' => "No channel details found.",
                'first_name.required_without_all' => "Cannot create a blank contact. Please fill in at least one contact field.",
                'phone_number.required_without_all' => "Please create contact with any of first name,phone number or email address.",
                'phone_number.required_with_all' => "Please select or enter phone type as well.",
                'email.required_without_all' => "Please create contact with any of first name,phone number or email address.",
                'email_type.required_with_all' => "Please select or enter email type as well.",
                'google_contact_resource_id.required' => "Please choose google contact to update.",

            ];
        return $error_messages;
    }
}

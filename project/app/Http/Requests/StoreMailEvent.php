<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMailEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'receiving_email' => 'required',
            'mail_driver' => 'required',
            'mail_host' => 'required',
            'mail_username' => 'required',
            'mail_password' => 'required',
            'mail_encryption' => 'required',
            'mail_from_name' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'channel_id.required' => "No channel details found.",
                'webhook_topic_id.required' => "No event details found.",

            ];
        return $error_messages;
    }
}

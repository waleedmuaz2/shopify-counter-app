<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMsExcelEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'drive_folder_id' => 'required',
            'microsoft_account_id' => 'required',
            'workbook_id' => 'required',
            'worksheet_id' => 'required',
            'drive_folder_name' => 'required',
            'workbook_name' => 'required',
            'worksheet_name' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'workbook_id.required' => "Select worksheet to continue.",
                'drive_folder_id.required' => "Select drive folder .",
                'sid.required' => "Please enter ACCOUNT SID.",
                'microsoft_account_id.required' => "No microsoft account details found.Please sign to continue",
                'channel_id.required' => "No channel details found.",
                'worksheet_id.required' => "Select worksheetto continue.",

            ];
        return $error_messages;
    }
}

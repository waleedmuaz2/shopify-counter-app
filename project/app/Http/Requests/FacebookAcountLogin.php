<?php

namespace App\Http\Requests;



class FacebookAcountLogin extends JsonFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'access_token' => 'required',
            'userID' => 'required',

        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'access_token.required' => "No accessToken found.",
                'userID.required' => "userID is required.",


            ];
        return $error_messages;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMysqlEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'columns_mapped_fields' => 'array',
            'where_clause_fields' => 'array',
            'mysql_connection_id' => 'required',
            'database_table_name' => 'required',

        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'columns_mapped_fields.array' => "Choose fields to map with database columns.",
                'where_clause_fields.array' => "Mysql conditions on table columns rows is incorrect.",
                'mysql_connection_id.required' => "No mysql account details found",
                'channel_id.required' => "No channel details found.",
                'database_table_name.required' => "Select database to continue.",

            ];
        return $error_messages;
    }
}

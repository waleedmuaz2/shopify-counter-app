<?php

namespace App\Http\Controllers;


use App\Entities\BoxLogs;
use App\Entities\PlansHistory;
use App\Entities\ChannelConfig;
use App\Entities\Tokens;
use App\Jobs\InstallAppNotification;
use App\Entities\OrderLogs;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Oseintow\Shopify\Facades\Shopify;
use App\Services\ShopifyService;
use App\Entities\Shop;
use App\Entities\PostCounter;
use App\Entities\Helper;
use DB;
use URL;

class ShopifyController extends Controller
{
    private $shopify_service;
    private $api_version;

    public function __construct(ShopifyService $shopify_service)
    {
        $this->shopify_service = $shopify_service;
        $this->api_version = config('shopify.api_version');
    }


    public
    function install_app(Request $request)
    {
        $shopUrl = $request->shop;
        if ($request->shop) {

            $scope = config('shopify.scope');
            $scope = explode(',', $scope);
            $redirectUrl = config('shopify.redirect_url');
            $shopify = Shopify::setShopUrl($shopUrl);

            // invalidate existing session & re-create one with a generated state
            session()->invalidate();
            session()->start();
            session()->put('auth_state', $state = uniqid());
            $url = $shopify->getAuthorizeUrl($scope, $redirectUrl, $state);
            return view('authorize', compact('url'));
        } else {
            return $this->badLogin('Store URL Missiing');
        }

    }


    public
    function shopify_callback(Request $request)
    {

        // NOTE: integrity of input/hmac is verified in VerifyShopifyRequest middleware class
        // verify state
        $state = $request->state;
        $expected_state = session()->remove('auth_state');
        if (empty($state) || $state != $expected_state) {
            // return $this->badLogin('invalid state');
        }
        // verify required input
        $shopUrl = $request->shop;
        $code = $request->code;
        if (empty($shopUrl) || empty($code)) {
            $this->badLogin('missing input');
        }
        // retrieve token and shop data
        $accessToken = Shopify::setShopUrl($shopUrl)->getAccessToken($code);
        if (!$accessToken || empty($accessToken))
            return $this->badLogin('Unable to retrieve shop access token.Please restart the app from apps listing.');
        $shop_data = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/api/$this->api_version/shop.json");
        if (!$shop_data || empty($shop_data)) {
            return $this->badLogin('unable to retrieve shop data');
        } else {
            session()->put('shopify_token', $accessToken);
            $token=$accessToken;
            \Illuminate\Support\Facades\DB::statement("UPDATE `access_tokens` SET `accessToken`='".$token."' WHERE `id` = 1");

            $shop_saved_object = $this->shop_data($shopUrl, $shop_data, $accessToken);
            if ($shop_saved_object && !$shop_saved_object->is_webhooks_added) {
                $this->shopify_service->verify_webhooks($shop_saved_object);
            }

        }
        $redirect_to = "https://" . $shopUrl . "/admin/apps/" . config('shopify.api_key') . "/" . config('shopify.directory_name') . "/main_app_page";
        return redirect($redirect_to);

    }

    private
    function shop_data($domain, $shop_data, $access_token)
    {
        try {
            $new_store = false;
            $shop = Shop::firstOrNew(array('myshopify_domain' => $domain));
            // if store installed the app for the first time
            if (empty($shop->id)) {
                $new_store = true;
            }
            $shop->shop_id = $shop_data['id'];
            $shop->name = $shop_data['name'];
            $shop->email = $shop_data['email'];
            $shop->customer_email = $shop_data['customer_email']??'';
            $shop->domain = $shop_data['domain'];
            $shop->myshopify_domain = $shop_data['myshopify_domain'];
            $shop->country = $shop_data['country'];
            $shop->address = $shop_data['address1'];
            $shop->address2 = $shop_data['address2']??"";
            $shop->zip = $shop_data['zip'];
            $shop->city = $shop_data['city'];
            $shop->province = $shop_data['province'];
            $shop->country_name = $shop_data['country_name'];
            $shop->country_code = $shop_data['country_code'];
            $shop->phone = $shop_data['phone'];
            $shop->currency = $shop_data['currency'];
            $shop->shop_owner = $shop_data['shop_owner'];
            $shop->timezone = $shop_data['timezone'];
            $shop->iana_timezone = $shop_data['iana_timezone'];
            $shop->access_token = $access_token;
            $shop->primary_locale = $shop_data['primary_locale'];
            $shop->uninstalled_at = null;
            $shop->save();
            session()->put('shop_id', $shop->id);
            session()->put('shop', $shop);

            return $shop;
        } catch (\Exception $e) {
            dd($e);
            return false;
        }

    }


    public
    function main_app_page()
    {
        $shop = 'viewcounterapp';
        $token = session()->get('shopify_token');
//        $token=DB::table('access_tokens')->where('id',1)->first()->accessToken;
        $query = array(
            "Content-type" => "application/json" 
        );
        $article_detail_array = [];
        $user_detail_array = [];
        $vistors=[];
        $data=[];
        $store_ids = PostCounter::all();
        $storeVistor=DB::table('post_counter')
            ->select('post_title', 'is_register','blog_id')
            ->groupByRaw('post_title')
            ->get();
        $i=1;
        foreach ($store_ids as $store_id) {
            $Get_article = Helper::shopify_call($token, $shop, "/admin/api/2021-01/blogs/" . $store_id->blog_id . "/articles/" . $store_id->post_title . ".json", array(), 'GET');
            $article_res = $Get_article['response'];
            $decode_article = json_decode($article_res);
            $article_detail_array = $decode_article;
            $Get_user = Helper::shopify_call($token, $shop, "/admin/api/2021-01/customers/" . $store_id->is_register . ".json", array(), 'GET');
            $user_res = $Get_user['response'];
            $decode_user =json_decode($user_res);
            $user_detail_array = $decode_user;
            if(isset($article_detail_array->errors)){
                dd($article_detail_array);
            }
            if (isset($user_detail_array->errors)) {
                $data []= [
                    'srNumber' => $i++,
                    'Blog_id' => $store_id->blog_id,
                    'post_id' => $store_id->post_title,
                    'post_title' => $article_detail_array->article->title,
                    'vistor_name' => "Guest",
                    'email' => "-",
                    'total_spent' => "-",
                    'created_at'   => $store_id->created_at,
                    'handle'=>  $article_detail_array->article->handle
                ];
            } else {
                $data []= [
                    'srNumber' => $i++,
                    'Blog_id' => $store_id->blog_id,
                    'post_id' => $store_id->post_title,
                    'post_title' => $article_detail_array->article->title,
                    'vistor_name' => $user_detail_array->customer->first_name . " " . $user_detail_array->customer->last_name,
                    'email' => $user_detail_array->customer->email,
                    'total_spent' => $user_detail_array->customer->total_spent . " " . $user_detail_array->customer->currency,
                    'created_at'   => $store_id->created_at,
                    'handle'=>  $article_detail_array->article->handle

                ];
            }
        }

        foreach ($storeVistor as $store_id) {
            $Get_article = Helper::shopify_call($token, $shop, "/admin/api/2021-01/blogs/" . $store_id->blog_id . "/articles/" . $store_id->post_title . ".json", array(), 'GET');
            $article_res = $Get_article['response'];
            $decode_article = json_decode($article_res);
            $article_detail_array = $decode_article;
                $vistors []= [
                    'Blog_id' => $store_id->blog_id,
                    'post_id' => $store_id->post_title,
                    'post_title' => $article_detail_array->article->title,
                    'total_guest_user'=>PostCounter::where('is_register',0)->where('post_title',$store_id->post_title)->count(),
                    'total_register_user'=>PostCounter::where('is_register','!=',0)->where('post_title',$store_id->post_title)->count(),
                    'total_user'=>PostCounter::where('post_title',$store_id->post_title)->count(),
                    'handle'=>  $article_detail_array->article->handle
                ];
            }
        $totalGuestUser=PostCounter::where('is_register',0)->count();
        $totalRegisterUser=PostCounter::where('is_register','!=',0)->count();
        $totalUser = PostCounter::count();
        $shop_id=session()->get('shop')->shop_id;

        $settings = DB::table('customize_counter_setting_place')->where('store_id',$shop_id)->first();
        return view("dashboard", compact('data','vistors','totalGuestUser','totalRegisterUser','totalUser','settings'));

    }
    public
    function removeApp()
    {
        $data = fisle_get_contents('php://input');
        $data = json_decode($data);
        if (!empty($data)) {
            $shop = Shop::where('shop_id', $data->id)->first();
            if ($shop) {
                $shop->is_deleted = 1;
                $shop->current_plan_type = null;
                $shop->is_webhooks_added = 0;
                $shop->uninstalled_at = \Carbon\Carbon::now();
                $shop->save();
                return response()->json(array('success' => true, 'message' => "received"), 200);
            }
        }

    }

    public function badLogin($message)
    {
        session()->invalidate();
        return view('errors.un_verrified_shopify_request', compact('message'));
    }


    public function un_verrified_shopify_request(Request $request)
    {
        $message = $request->message ?? "";
        return view('errors.un_verrified_shopify_request', compact('message'));
    }
    public function Setting(Request $request){
        $shop_id=session()->get('shop')->shop_id;
        DB::table('customize_counter_setting_place')->where('store_id',$shop_id)->update(
            [
                'store_id'=>$shop_id,
                'listing_page_class'=>$request->listing_class,
                'property_listing'=>$request->listing_page_options,
                'detail_page_class'=>$request->detail_class,
                'property_detail'=>$request->detial_page_options,
                'detail_icon_url'=> $request->detail_icon_url, //<img src='$request->detail_icon_url' alt='icon' style='height:50px;width: 50px'  >",
                'listing_icon_url'=> $request->listing_icon_url,  //" <img src='$request->listing_icon_url' alt='icon'  style='height:50px;width: 50px'  > ",
            ]
        );
        return back();
    }
    public function post_detail(Request $request,$id,$title){
        $shop = 'viewcounterapp';
        $token = session()->get('shopify_token');
        $query = array(
            "Content-type" => "application/json"
        );
        $article_detail_array = [];
        $user_detail_array = [];
        $vistors=[];
        $data=[];


        $store_ids=DB::table('post_counter')
            ->select('post_title', 'is_register','blog_id','created_at',DB::raw('COUNT(is_register) as totalUser'))
            ->where('post_title',$id)
            ->groupByRaw('is_register')
            ->orderByRaw('created_at','desc')
            ->get();
        $i=1;
        foreach ($store_ids as $store_id) {
            $Get_user = Helper::shopify_call($token, $shop, "/admin/api/2021-01/customers/" . $store_id->is_register . ".json", array(), 'GET');
            $user_res = $Get_user['response'];
            $decode_user = json_decode($user_res);
            $user_detail_array = $decode_user;
            if (isset($user_detail_array->errors)) {
                $data []= [
                    'srNumber' => $i++,
                    'vistor_name' => "Guest",
                    'counter'=> $store_id->totalUser,
                    'created_at'   => $store_id->created_at,
                ];
            } else {
                $data []= [
                    'srNumber' => $i++,
                    'vistor_name' => $user_detail_array->customer->first_name . " " . $user_detail_array->customer->last_name,
                    'counter'=> $store_id->totalUser,
                    'created_at'   => $store_id->created_at,
                ];
            }
        }
        return view('datatable',compact('data','title'));
    }

}

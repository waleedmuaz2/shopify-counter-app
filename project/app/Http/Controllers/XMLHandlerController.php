<?php

namespace App\Http\Controllers;


// use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
 use Maatwebsite\Excel\Facades\Excel;
use Marquine\Etl\Etl;
use Symfony\Component\Console\Input\Input;

class XMLHandlerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $path   =   public_path('EarnMoreService.csv');

        $file = fopen($path,"r");
        $count = 0;
        $header = '';
        $totalRows = [];
        while ($line = fgetcsv($file)){
            if ($count ==0){
                $header=$line;
            }
            if ($count>0){
                foreach ($line as $key => $column){

                    $totalRows[]=$line[$key];
                    /*$totalRows[$count]['querystring']=parse_url($line[$key], PHP_URL_QUERY);*/

                    /*if (!empty($totalRows[$count]['url'])){
                        $partner = Partner::where('url',$totalRows[$count]['url'])->where('query',$totalRows[$count]['querystring'])->first();

                        if (!$partner){
                            Partner::create([
                                'url'   =>   $totalRows[$count]['url'],
                                'query'   =>   $totalRows[$count]['querystring'],
                            ]);
                        }
                    }*/
                }
            }
            $count++;
        }
        fclose($file);
        dd($totalRows);

        /*$array=[];
        $partners   =   Partner::groupBy('url')->select('url')->get();

        foreach ($partners->toArray() as $ar){
            echo $ar['url'].'<br>';
        }
        dd('hello');
        foreach ($partners as $key=>$partner){
            $query  =   Partner::where('url',$partner->url)->select('query')->get();

            foreach ($query as $j=>$q){
                $partners[$key]->query_string  =   $q->query;
            }

        }
        dd($partners->toArray());*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = [
                "facilityId" => "Select Language",
                "customerId" => "",
                "orderNumber" =>  " ",
                "orderDate" => "",
                "shipTo" => [
                    "addressLine1"=>'addressLine1',
                    "addressLine2"=>'addressLine2',
                    "addressLine3"=>'addressLine3',
                    "postCode"=>'addressLine3',
                    "city"=>'addressLine3',
                    "state"=>'addressLine3',
                    "countryCode"=>'addressLine3',
                    "name"=>'addressLine3',
                    "phoneNumber"=>'addressLine3',
                    "email"=>'addressLine3',
                ],
                "billTo" => [
                    "addressLine1"=>'addressLine1',
                    "addressLine2"=>'addressLine2',
                    "addressLine3"=>'addressLine3',
                    "postCode"=>'addressLine3',
                    "city"=>'addressLine3',
                    "state"=>'addressLine3',
                    "countryCode"=>'addressLine3',
                    "name"=>'addressLine3',
                    "phoneNumber"=>'addressLine3',
                    "email"=>'addressLine3',
                ],
                "orderLines" => [
                    "line"=>[
                        "lineNumber" =>"lineNumber",
                        "itemNumber" =>"itemNumber",
                        "orderedQuantity" =>"orderedQuantity",
                        "inventoryStatus" =>"inventoryStatus",
                        "unitPrice" =>"unitPrice",
                        "unitPriceCurrency" =>"2unitPriceCurrency",
                    ],
                    "line"=>[
                        "lineNumber" =>"1lineNumber",
                        "itemNumber" =>"1itemNumber",
                        "orderedQuantity" =>"1orderedQuantity",
                        "inventoryStatus" =>"inventoryStatus",
                        "unitPrice" =>"unitPrice",
                        "unitPriceCurrency" =>"unitPriceCurrency",
                    ],
                ],
        ];
        $xml = new \SimpleXMLElement('<root/>');
        array_walk_recursive($array, array ($xml, 'addChild'));
        /*print $xml->asXML();*/

		$xml_datos  =   $xml->asXML();
		$output = \Illuminate\Support\Facades\View::make('xml')->with(compact('array'))->render();

		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" .$output;

		$response = Response::create($xml, 200);
		$response->header('Content-Type', 'text/xml');
		$response->header('Cache-Control', 'public');
		$response->header('Content-Description', 'File Transfer');
		$response->header('Content-Disposition', 'attachment; filename=' . 'xml.xml' . '');
		$response->header('Content-Transfer-Encoding', 'binary');

		return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        /*dd($request->all());*/
        $array   =   [
            'bk1',
            'bk2',
            'bk3',
            'bk4',
            'bk5',
            'bk6',
            'bk7',
            'bk8',
            'bk9',
            'bk10',
            'bk11',
            'bk12',
            'bk13',
            'bk14',
            'bk15',
            'bk16',
            'bk17',
            'bk18',
            'failed',
            'throttle',
            'reset',
            'sent',
            'throttled',
            'token',
            'user',
            'exists',
            'not_regex',
            'numeric',
            'password',
            'present',
            'regex',
            'required',
            'string',
            'unique',
            'pls_resolve_errors',
            'confirmation_email_sent',
            'user_created_successfully',
            'pls_confirm_email',
            'incorrect_credentials_entered',
            'email_not_found',
            'code_send_to_email',
            'password_changed_successfully',
            'code_incorrect',
            'user_registered_successfully',
            'user_logged_in_successfully',
            'email_does_not_match',
            'accepted_friend_request',
            'sent_friend_request',
            'is_mark_are_you_safe',
            'is_marked_safe',
            'want_to_add_you_as',
            'list',
            'accepted_your_request',
            'accepted_your',
            'request',
            'profile_updated_successfully',
            'sent_agreement_reached',
            'pls_enter_correct_type',
            'user_package_not_available',
            'transaction_successful',
            'user_not_ok_friend',
            'user_deleted_successfully',
            'user_is_not_friend',
            'ping_time_updated_successfully',
            'notification_sent_success',
            'request_has_been_set',
            'logout_success',
            'all_languages',
            'translations',
            'messages',
            'message_success',
            'action_not_found',
            'user_inbox',
            'notification_marked_read',
            'notification_list',
            'all_packages',
            'pls_enter_country_code',
            'user_mobile',
            'already_registered_to_lifesign',
            'user_success_referred',
            'sos_notification',
            'sos_notification_success',
            'no_sos_friends',
            'sos_list',
            'sos_reply_success',
            'this_sos_does_not_belong',
            'you_have_exceeded_max_friend',
            'user_already_friend',
            'friend_request_success',
            'pending_friend_requests',
            'request_success',
            'friend_request_not_found',
            'friend_request_rejected',
            'incorrect_user_friends',
            'friends_list_ping',
            'success_for_i_am_safe',
            'no_notifications',
            'friends_list',
            'incorrect_type_entered',
            'successfully_updated',
            'request_success_sent',
            'nick_name_success',
            'user_not_friend',
            'settings_updated',
        ];

        $data = [];
        /*dd(count(file($request->file)));*/
        $file   =   file($request->file);
        $file   =   json_encode($file);
        dd(json_decode($file));
        foreach(file($request->file) as $line) {
            echo $line.'<br>';
        }

        /*for ($i=0;$i<count($array);$i++){
            $total_data[$array[$i]]   =   $data[$i];
        }
        dd($total_data);*/



    }

    public function store_old(Request $request)
    {
        /*dd($request->all());*/
        $array   =   [
            'bk1',
            'bk2',
            'bk3',
            'bk4',
            'bk5',
            'bk6',
            'bk7',
            'bk8',
            'bk9',
            'bk10',
            'bk11',
            'bk12',
            'bk13',
            'bk14',
            'bk15',
            'bk16',
            'bk17',
            'bk18',
            'failed',
            'throttle',
            'reset',
            'sent',
            'throttled',
            'token',
            'user',
            'exists',
            'not_regex',
            'numeric',
            'password',
            'present',
            'regex',
            'required',
            'string',
            'unique',
            'pls_resolve_errors',
            'confirmation_email_sent',
            'user_created_successfully',
            'pls_confirm_email',
            'incorrect_credentials_entered',
            'email_not_found',
            'code_send_to_email',
            'password_changed_successfully',
            'code_incorrect',
            'user_registered_successfully',
            'user_logged_in_successfully',
            'email_does_not_match',
            'accepted_friend_request',
            'sent_friend_request',
            'is_mark_are_you_safe',
            'is_marked_safe',
            'want_to_add_you_as',
            'list',
            'accepted_your_request',
            'accepted_your',
            'request',
            'profile_updated_successfully',
            'sent_agreement_reached',
            'pls_enter_correct_type',
            'user_package_not_available',
            'transaction_successful',
            'user_not_ok_friend',
            'user_deleted_successfully',
            'user_is_not_friend',
            'ping_time_updated_successfully',
            'notification_sent_success',
            'request_has_been_set',
            'logout_success',
            'all_languages',
            'translations',
            'messages',
            'message_success',
            'action_not_found',
            'user_inbox',
            'notification_marked_read',
            'notification_list',
            'all_packages',
            'pls_enter_country_code',
            'user_mobile',
            'already_registered_to_lifesign',
            'user_success_referred',
            'sos_notification',
            'sos_notification_success',
            'no_sos_friends',
            'sos_list',
            'sos_reply_success',
            'this_sos_does_not_belong',
            'you_have_exceeded_max_friend',
            'user_already_friend',
            'friend_request_success',
            'pending_friend_requests',
            'request_success',
            'friend_request_not_found',
            'friend_request_rejected',
            'incorrect_user_friends',
            'friends_list_ping',
            'success_for_i_am_safe',
            'no_notifications',
            'friends_list',
            'incorrect_type_entered',
            'successfully_updated',
            'request_success_sent',
            'nick_name_success',
            'user_not_friend',
            'settings_updated',
        ];

        $data = [];
        /*dd(count(file($request->file)));*/
        foreach(file($request->file) as $line) {
            $data[] =   trim(preg_replace('/\s\s+/', ' ', $line));

        }

        for ($i=0;$i<count($array);$i++){
            $total_data[$array[$i]]   =   $data[$i];
        }
        dd($total_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function urlToXML(){

       $urls ='[
                   {
                      "city":"Albany",
                      "url":"https://news.google.com/rss/search?q=Charlotte%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Charlotte",
                      "url":"https://news.google.com/rss/search?q=Charlotte%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Chicago",
                      "url":"https://news.google.com/rss/search?q=Chicago%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Atlanta",
                      "url":"https://news.google.com/rss/search?q=Atlanta%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Housten",
                      "url":"https://news.google.com/rss/search?q=Housten%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Columbus",
                      "url":"https://news.google.com/rss/search?q=Columbus%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"NewYork",
                      "url":"https://news.google.com/rss/search?q=NewYork%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"San Antonio",
                      "url":"https://news.google.com/rss/search?q=San%20Antonio%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Dallas",
                      "url":"https://news.google.com/rss/search?q=Dallas%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Los Angeles",
                      "url":"https://news.google.com/rss/search?q=Los%20Angeles%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Boston",
                      "url":"https://news.google.com/rss/search?q=Boston%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Denver",
                      "url":"https://news.google.com/rss/search?q=Denver%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Phoenix",
                      "url":"https://news.google.com/rss/search?q=Phoenix%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   },
                   {
                      "city":"Silicon Valley",
                      "url":"https://news.google.com/rss/search?q=Silicon%20Valley%20Business%20Journal%20local%20News&hl=en-US&gl=US&ceid=US%3Aen"
                   }
                ]';
        foreach (json_decode($urls) as $url){
            $city=$url->city;
            $response_xml_data = file_get_contents($url->url);
            $data = simplexml_load_string($response_xml_data);
            foreach ($data->channel->item as $items)
            {
                DB::insert('insert into newsAlert (title,link,pubDate,description,source,mainTitle) VALUE (
                    "'.htmlspecialchars($items->title).'",
                    "'.htmlspecialchars($items->link).'",
                    "'.htmlspecialchars($items->pubDate).'",
                    "'.htmlspecialchars($items->description).'",
                    "'.htmlspecialchars($items->source).'",
                    "'.$city.'"
                )');
            }
        }

    }
}

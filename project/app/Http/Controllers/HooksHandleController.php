<?php

namespace App\Http\Controllers;

use App\Entities\IncomingShopifyWebhook;
use App\Entities\OrderLogs;
use App\Entities\Shop;
use App\Entities\PostCounter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class HooksHandleController extends Controller
{

    public function CountView(Request $request)
    {
        PostCounter::create([
            'post_title'     => $request->title,
            'is_register'    => $request->regitser_user,
            'blog_id'       => $request->blog_id,
        ]);
        return 1;

    }
    public function CountDisplay(Request $request)
    {
        $post_view = PostCounter::where('post_title',$request->post_title_id)->count();
        $settings = DB::table('customize_counter_setting_place')->first();
        $data =[
                'counter' =>$post_view,
                'setting'=>$settings
            ];
        return $data;
    }
}

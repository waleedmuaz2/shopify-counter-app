<?php

namespace App\Http\Controllers;

use App\OrderLogs;
use Illuminate\Http\Request;

class OrderLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderLogs  $orderLogs
     * @return \Illuminate\Http\Response
     */
    public function show(OrderLogs $orderLogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderLogs  $orderLogs
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderLogs $orderLogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderLogs  $orderLogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderLogs $orderLogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderLogs  $orderLogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderLogs $orderLogs)
    {
        //
    }
}

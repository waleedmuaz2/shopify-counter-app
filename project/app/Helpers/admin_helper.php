<?php

use Carbon\Carbon;
use Illuminate\Support\Str;

if (!function_exists('getCreatedAtAttribute')) {

    function getCreatedAtAttribute($date_time, $shop)
    {
        $date_time = Carbon::createFromTimestamp(strtotime($date_time));
        if ($shop && $shop->timezone)
            $date_time = $date_time->timezone($shop->timezone);
        $date_time = $date_time->toDateTimeString(); //remove this one if u want to return Carbon object
        return $date_time;
    }
}
if (!function_exists('XMLArray')) {
    function XMLArray($array, $key, $value)
    {
        $results = array();
        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, XMLArray($subarray, $key, $value));
            }
        }

        return $results;
    }
}
if (!function_exists('getDateWithTimeZone')) {

    function getDateWithTimeZone($date_time, $timezone=null)
    {
        $date_time = Carbon::createFromTimestamp(strtotime($date_time));
        if ($timezone)
            $date_time = $date_time->timezone($timezone);
        $date_time = $date_time->toDateTimeString(); //remove this one if u want to return Carbon object
        return $date_time;
    }
}

if (!function_exists('allowed_webhooks_tasks')) {

    function allowed_webhooks_tasks($user_plan)
    {
        $tasks = 0;
        if ($user_plan == 'basic') {
            $tasks = 200;
        } elseif ($user_plan == 'professional') {
            $tasks = 10000;
        } elseif ($user_plan == 'elite') {
            $tasks = 20000;
        }
        return $tasks;
    }
}

if (!function_exists('make_domain_url')) {

    function make_domain_url($domain)
    {
        return parse_url($domain, PHP_URL_SCHEME) ? $domain : "https://$domain";

    }
}
if (!function_exists('store_admin_panel_url')) {

    function store_admin_panel_url($shop)
    {
        $domain = parse_url($shop->myshopify_domain, PHP_URL_SCHEME) ? $shop->myshopify_domain : "https://$shop->myshopify_domain";
        return $domain . "/admin";

    }
}
if (!function_exists('store_webhook_status')) {

    function store_webhook_status($shop_id, $webhook_topic_id)
    {
        return \App\Entities\StoreWebhook::where('shop_id', $shop_id)
            ->where('webhook_topic_id', $webhook_topic_id)
            ->first();

    }
}
if (!function_exists('livid_storage')) {

    function livid_storage($dir)
    {
        return storage_path('app/public/' . $dir);

    }
}

if (!function_exists('livid_storage_path')) {
    function livid_storage_path($store_id, $folderame)
    {
        $basepath = livid_storage('stores/') . $store_id . "/" . $folderame . "/";
        if (!file_exists($basepath)) {
            File::makeDirectory($basepath, 0777, true);
        }
        return $basepath;
    }
}
if (!function_exists('get_asset_components')) {

    function get_asset_components($key, $type)
    {
        $components = config("asset.components.{$key}");
        return $components[$type];
    }
}
function humanDateFormat($date)
{
    if (!empty($date)) {
        return \Carbon\Carbon::parse($date)->diffForHumans();
    } else {
        return "";
    }
}

if (!function_exists('webhookEventNameByTopicName')) {

    function webhookEventNameByTopicName($topic_name)
    {
        $event_name = \App\Entities\WebhookTopic::where('topic_name', $topic_name)
            ->join('webhook_events', 'webhook_events.id', '=', 'webhook_topics.webhook_event_id')
            ->groupBy('webhook_events.event_name')
            ->pluck('webhook_events.event_name');

        return $event_name[0] ?? '';
    }
}

if (!function_exists('topicNameById')) {

    function topicNameById($id)
    {
        $topic_name = \App\Entities\WebhookTopic::where('id', $id)
            ->pluck('topic_name');

        return $topic_name[0] ?? '';
    }
}

if (!function_exists('webhookEventIdByTopicId')) {

    function webhookEventIdByTopicId($id)
    {
        $webhook_event_id = \App\Entities\WebhookTopic::where('id', $id)
            ->pluck('webhook_event_id');
        return $webhook_event_id[0] ?? 0;
    }


    if (!function_exists('topicTypeByTopicName')) {
        function topicTypeByTopicName($topic_name)
        {
            if (Str::contains($topic_name, ['/create', '/edited', '/connect'])) {
                $topic_type = "create";
            } elseif (Str::contains($topic_name, '/delete')) {
                $topic_type = "delete";
            } else {
                $topic_type = "update";
            }
            return $topic_type;
        }
    }

}
if (!function_exists('ConfigShopify')) {

    function ConfigShopify()
    {
        $config = array(
            'ShopUrl' => 'https://dhlstore123.myshopify.com/',
            'ApiKey' => 'abf9e827975166b6bdc26ab120575f4f',
            'Password' => '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',
        );
       return PHPShopify\ShopifySDK::config($config);
        /*  \PHPShopify\AuthHelper::createAuthRequest($scopes, $redirectUrl);*/
    }
}
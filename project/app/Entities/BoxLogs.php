<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BoxLogs extends Model
{
    protected $fillable=['orderID','caseID','trackingNumber','cartonType','cartonCode','cartonLenght','cartonHeight','cartonWidth','cartonWeight','lineNumber','itemNumber','orderedQuantity','shippedQuantity','serialNumbers'];
}

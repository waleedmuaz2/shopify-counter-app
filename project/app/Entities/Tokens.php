<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Tokens extends Model{

    protected $table="access_tokens";
    protected $fillable=['id','accessToken'];
}
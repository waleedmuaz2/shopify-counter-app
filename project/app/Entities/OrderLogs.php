<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderLogs extends Model
{
    protected $fillable=['order_id','order_name','order_price','date_time','customer_name','xml_status_import','xml_status_export','json_array','messageDate','customerId','receiptDate','receiptNumber','receiptType'];
    public  function BoxLogs(){
        return $this->hasMany(BoxLogs::class,'orderID','order_id');
    }
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class IncomingShopifyWebhook extends Model
{
    public static function handle_dublicate_webhooks($shop, $event_topic, $data)
    {
        if ($event_topic == 'orders/edited') {
            $data = $data['order_edit'];
        }
        if (Str::contains($event_topic, 'locales')) {
            return true;
        }
        $object_id = $data['id'] ?? $data['inventory_item_id'] ?? 0;
        $shopify_updated_at = $data['updated_at'] ?? '0000-00-00 00:00:00';
        try {
            $log = new IncomingShopifyWebhook();
            $log->object_id = $object_id;
            $log->shop_id = $shop->shop_id;
            $log->topic_name = $event_topic;
            $log->shopify_updated_at = $shopify_updated_at;
            $log->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}

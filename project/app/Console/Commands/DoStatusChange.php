<?php

namespace App\Console\Commands;

use App\Entities\BoxLogs;
use App\Entities\Helper;
use App\Entities\OrderLogs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\DeclareDeclare;
use SimpleXMLElement;
class DoStatusChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:DoStatusChange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \Log::info('Start');
        $shop = env('SFTP_C_STORENAME','dhlstore123');
        $token = session()->get('shopify_token');
        $query = array(
            "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
        );
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        //dd($file_list[22]);
        //dd(substr($file_list[22],4,-35));
        foreach ($file_list as $key => $value) {
            if("DOSTATUS"==substr($value,4,-35)){
                $str=str_replace("out/","",$value);
                $location='project/storage/app/public'.'/'.$str;
                $var=Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
                $xmls = new SimpleXMLElement(file_get_contents($location));
                $array = json_decode(json_encode($xmls));
                $order_id = json_decode(json_encode($xmls))->orderNumber;//$array->orderNumber;
                $fulfillment_order_id = OrderLogs::where('order_id',$order_id)->first()->order_name;
                $Get_FullFillMent_ID = Helper::shopify_call($token, $shop, "/admin/api/2019-10/orders/".$fulfillment_order_id."/fulfillments.json", array(), 'GET');

                $Get_FullFillMent_ID = $Get_FullFillMent_ID['response'];
                $FullFillMent_ID_Response = json_decode($Get_FullFillMent_ID);
                if (!$FullFillMent_ID_Response){
                    return 1;
                }
                $fulfillment_id = $FullFillMent_ID_Response->fulfillments[0]->id;
                $label = "label_printed";

                if(!is_null($array->OrderStatus)){

                    switch ($array->OrderStatus) {
                        case "Started":
                            $label  = "label_printed";
                            break;
                        case "Acknowledged":
                            $label  = "label_printed";
                            break;
                        case "Cancelled":
                            $label= "failure";
                            break;
                        case "Delivered":
                            $label= "delivered";
                            break;
                        default:
                            $label= "label_printed";
                    }
                }
                $modify_data =   array( "event" => array("status" =>$label));
                $modified_product =Helper::shopify_call($token, $shop, "/admin/api/2021-01/orders/".$fulfillment_order_id."/fulfillments/".$fulfillment_id."/events.json", $modify_data, 'POST');
                $modified_product_response = $modified_product['response'];
                $modified_product_response = json_decode($modified_product_response);
                if (is_string($order_id)){
                    OrderLogs::where('order_id',$order_id)->update([
                        'Order_Status' => $array->OrderStatus,
                        'Reason_Code'=> $array->reasonCode,
                    ]);
                }
            }
            //Storage::disk('remote-sftp')->delete($value);
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Entities\Helper;
use App\Entities\OrderLogs;
use App\Mail\MailDHL;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Oseintow\Shopify\Exceptions\ShopifyApiException;
use SimpleXMLElement;
use App\Entities\boxLogs;

class GetXMLServerDHL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:getXML';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Start');
        $shop = env('SFTP_C_STORENAME','dhlstore123');
        $token = session()->get('shopify_token');
        $query = array(
            "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
        );
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        foreach ($file_list as $key => $value) {
            if("DOCONFIRM"==substr($value,4,-35)){
               // dd($value);
                $str=str_replace("out/","",$value);
                $location='project/storage/app/public'.'/'.$str;
                $var=Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));

                $xmls = new SimpleXMLElement(file_get_contents($location));
                $array = json_decode(json_encode($xmls));
                //dd($array);
                $order_id = json_decode(json_encode($xmls))->orders->order->orderNumber;
                $checkIfArray = is_array($array->pallets->pallet->orders->order->boxes->box);
                //dd($checkIfArray);
                if($checkIfArray==false){
                    //dd('good');
                    foreach ($array->pallets->pallet->orders->order->boxes as $box){
                        boxLogs::create([
                            'orderID'=>$order_id,
                            'caseID'=>(isset($box->caseID))?$box->caseID : "",
                            'trackingNumber'=>$box->trackingNumber,
                            'cartonType'=>$box->cartonType,
                            'cartonCode'=>$box->cartonCode,
                            'cartonLenght'=>$box->cartonLenght,
                            'cartonHeight'=>$box->cartonHeight,
                            'cartonWidth'=>$box->cartonWidth,
                            'cartonWeight'=>$box->cartonWeight,
                            'lineNumber'=>$box->orderLines->line->lineNumber,
                            'itemNumber'=>$box->orderLines->line->itemNumber,
                            'orderedQuantity'=>$box->orderLines->line->orderedQuantity,
                            'shippedQuantity'=>$box->orderLines->line->shippedQuantity,
                            'serialNumbers'=>(isset($box->orderLines->line->serialNumbers) ? $box->orderLines->line->serialNumbers : "" ),
                        ]);
                        $fulfillment_order_id=0;
                        $fulfillment_order_id_order = OrderLogs::where('order_id',$order_id)->first();
                        //dd($fulfillment_order_id_order);
                        if (!is_null($fulfillment_order_id_order)) {
                            $fulfillment_order_id = $fulfillment_order_id_order->order_name;


                            $Get_FullFillMent_ID = Helper::shopify_call($token, $shop, "/admin/api/2019-10/orders/" . $fulfillment_order_id . "/fulfillments.json", array(), 'GET');
                            $Get_FullFillMent_ID = $Get_FullFillMent_ID['response'];
                            $FullFillMent_ID_Response = \GuzzleHttp\json_decode($Get_FullFillMent_ID);
                            if (!isset($FullFillMent_ID_Response->errors)) {


                                $fulfillment_id = $FullFillMent_ID_Response->fulfillments[0]->id;
                                $modify_data = array("fulfillment" => array("tracking_number" => $box->trackingNumber,));
                                $modified_product = Helper::shopify_call($token, $shop, "/admin/api/2021-01/orders/" . $fulfillment_order_id . "/fulfillments/" . $fulfillment_id . ".json", $modify_data, 'PUT');
                                $modified_product_response = $modified_product['response'];
                                $modified_product_response = \GuzzleHttp\json_decode($modified_product_response);
                                //dd($modified_product_response);
                            }
                        }
                    }
                }else{
                   // dd('3');
                    foreach ($array->pallets->pallet->orders->order->boxes->box as $box) {
                        boxLogs::create([
                            'orderID' => $order_id,
                            'caseID' => (isset($box->caseID)) ? $box->caseID : "",
                            'trackingNumber' => $box->trackingNumber,
//                            'cartonType'=>$box->cartonType,
//                            'cartonCode'=>$box->cartonCode,
                            'cartonLenght' => $box->cartonLenght,
                            'cartonHeight' => $box->cartonHeight,
                            'cartonWidth' => $box->cartonWidth,
                            'cartonWeight' => $box->cartonWeight,
                            'lineNumber' => $box->orderLines->line->lineNumber,
                            'itemNumber' => $box->orderLines->line->itemNumber,
                            'orderedQuantity' => $box->orderLines->line->orderedQuantity,
                            'shippedQuantity' => $box->orderLines->line->shippedQuantity,
                            'serialNumbers' => (isset($box->orderLines->line->serialNumbers) ? $box->orderLines->line->serialNumbers : ""),
                        ]);

                        //$fulfillment_order_id = OrderLogs::where('order_id',$order_id)->first()->order_name;
                        //dd($fulfillment_order_id);
                        $fulfillment_order_id = 0;
                        $fulfillment_order_id_order = OrderLogs::where('order_id', $order_id)->first();
                        if (!is_null($fulfillment_order_id_order)){
                            $fulfillment_order_id = $fulfillment_order_id_order->order_name;

                        $Get_FullFillMent_ID = Helper::shopify_call($token, $shop, "/admin/api/2019-10/orders/" . $fulfillment_order_id . "/fulfillments.json", array(), 'GET');
                        $Get_FullFillMent_ID = $Get_FullFillMent_ID['response'];
                        $FullFillMent_ID_Response = \GuzzleHttp\json_decode($Get_FullFillMent_ID);
                        //dd($FullFillMent_ID_Response->errors);
                        if (!isset($FullFillMent_ID_Response->errors)) {

                            $fulfillment_id = $FullFillMent_ID_Response->fulfillments[0]->id;
                            $modify_data = array("fulfillment" => array("tracking_number" => $box->trackingNumber,));
                            $modified_product = Helper::shopify_call($token, $shop, "/admin/api/2021-01/orders/" . $fulfillment_order_id . "/fulfillments/" . $fulfillment_id . ".json", $modify_data, 'PUT');
                            $modified_product_response = $modified_product['response'];
                            $modified_product_response = \GuzzleHttp\json_decode($modified_product_response);
                            //dd($modified_product_response);
                        }
                    }
                    }
                    OrderLogs::where('order_id',$order_id)->update([
                        'facilityId'=>$array->facilityId,
                        'messageDate'=>$array->messageDate,
                        'palletID'=>$array->pallets->pallet->palletID,
                        'orderNumber'=>$array->pallets->pallet->orders->order->orderNumber,
                        'carrierName'=>$array->pallets->pallet->orders->order->carrierName,
                        'DHL_created_file'=>$str,
                        'xml_status_import'=>'1'
                    ]);
                    $orders =Helper::shopify_call($token, $shop, '/admin/oauth/access_scopes.json', array(), 'GET',$query);
                }
//                Storage::disk('remote-sftp')->delete($value);
            }
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwoColumnCustomizeCounterSettingPlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customize_counter_setting_place', function (Blueprint $table) {
            $table->string('detail_icon_url')->nullable();
            $table->string('listing_icon_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customize_counter_setting_place', function (Blueprint $table) {
            $table->dropColumn('detail_icon_url');
            $table->dropColumn('listing_icon_url');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostCounterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_counter', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('post_title')->nullable();
            $table->string('is_register')->nullable();
            $table->string('blog_id')->nullable();
            $table->string('ip')->nullable();
                        $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_counter');
    }
}

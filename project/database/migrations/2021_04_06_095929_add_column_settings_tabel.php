<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSettingsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customize_counter_setting_place', function (Blueprint $table) {
            $table->string('property_listing')->nullable();
            $table->string('property_detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customize_counter_setting_place', function (Blueprint $table) {
            $table->dropColumn("property_detail");
            $table->dropColumn("property_listing");
        });
    }
}

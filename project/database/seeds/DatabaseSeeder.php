<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $this->call(ChannelsSeeder::class);
//         $this->call(WebhookEventsSeeder::class);
//         $this->call(WebhookTopicsSeeder::class);
//         $this->call(AdminUserSeeder::class);
//         $this->call(FacebookEventsSeeder::class);
//         $this->call(TwitterEventsSeeder::class);
//         $this->call(GoogleContactEvents::class);
         $this->call(ChannelEventsSeeder::class);

    }
}

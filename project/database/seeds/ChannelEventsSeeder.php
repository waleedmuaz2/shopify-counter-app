<?php

use Illuminate\Database\Seeder;

class ChannelEventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*    [
                    'name' => 'Create Contact',
                    'slug' => 'create_contact',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Update Contact',
                    'slug' => 'update_contact',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Create Product',
                    'slug' => 'create_product',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Update Product',
                    'slug' => 'update_product',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Create Line Item',
                    'slug' => 'create_line_item',
                    'status' => 0,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Update Line Item',
                    'slug' => 'update_line_item',
                    'status' => 0,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Create Ticket',
                    'slug' => 'create_ticket',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Update Ticket',
                    'slug' => 'update_ticket',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Create Deal',
                    'slug' => 'create_deal',
                    'status' => 1,
                    'channel_id' => 13,
                ],
                [
                    'name' => 'Update Deal',
                    'slug' => 'update_deal',
                    'status' => 1,
                    'channel_id' => 13,
                ],

    // mailchimp Events Seeders
                [
                    'name' => 'Add/update Subscriber',
                    'slug' => 'create_subscriber',
                    'status' => 1,
                    'channel_id' => 14,
                ],

                [
                    'name' => 'Add Subscriber To Tag',
                    'slug' => 'add_subscriber_to_tag',
                    'status' => 1,
                    'channel_id' => 14,
                ],
                [
                    'name' => 'Remove Subscriber To Tag',
                    'slug' => 'remove_subscriber_to_tag',
                    'status' => 1,
                    'channel_id' => 14,
                ],
                [
                    'name' => 'Create Campaign ',
                    'slug' => 'create_campaign',
                    'status' => 1,
                    'channel_id' => 14,
                ], [
                    'name' => 'Send Campaign ',
                    'slug' => 'send_campaign',
                    'status' => 1,
                    'channel_id' => 14,
                ],

    //        salesforce events starts here
                [
                    'name' => 'Create Record ',
                    'slug' => 'create_record',
                    'status' => 1,
                    'channel_id' => 15,
                ], [
                    'name' => 'Update Record ',
                    'slug' => 'update_record',
                    'status' => 1,
                    'channel_id' => 15,
                ]
                ,[
                    'name' => 'Add Lead to Campaign',
                    'slug' => 'add_lead_to_campaign',
                    'status'=>0,
                    'channel_id'=>15,
                ]
                ,[
                    'name' => 'Add Contact to Campaign',
                    'slug' => 'add_contact_to_campaign',
                    'status'=>0,
                    'channel_id'=>15,
                ],
*/

            [
                'name' => 'Create Customer',
                'slug' => 'create_customer',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Update Customer',
                'slug' => 'update_customer',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Product or Service',
                'slug' => 'create_product_or_service',
                'status' => 1,
                'channel_id' => 16,
            ],
             [
                'name' => 'Create Invoice',
                'slug' => 'create_invoice',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Send Invoice',
                'slug' => 'send_invoice',
                'status' => 1,
                'channel_id' => 16,
            ],

            [
                'name' => 'Create Sales Receipt',
                'slug' => 'create_sales_receipt',
                'status' => 1,
                'channel_id' => 16,
            ],[
                'name' => 'Create Purchase Order',
                'slug' => 'create_purchase_order',
                'status' => 1,
                'channel_id' => 16,
            ],


            [
                'name' => 'Create Credit Memo',
                'slug' => 'create_credit_memo',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Refund Receipt',
                'slug' => 'create_refund_receipt',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Estimate',
                'slug' => 'create_estimate',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Payment',
                'slug' => 'create_payment',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Send Payment',
                'slug' => 'send_payment',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Void Payment',
                'slug' => 'void_payment',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Bill (Account Based)',
                'slug' => 'create_bill_account_based',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Bill Payment',
                'slug' => 'create_bill_payment',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Credit Card Payment',
                'slug' => 'create_credit_card_payment',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create an Employee',
                'slug' => 'create_employee',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Time Activity',
                'slug' => 'create_time_activity',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Journal Entry',
                'slug' => 'create_journal_entry',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Update Invoice',
                'slug' => 'update_invoice',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Department',
                'slug' => 'create_department',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create a Purchase',
                'slug' => 'create_purchase',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Tax Agency',
                'slug' => 'create_tax_agency',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Vendor Credit (with line items support i-e Item Based Expense Line)',
                'slug' => 'create_vendor_credit_item_based',
                'status' => 1,
                'channel_id' => 16,
            ],

            [
                'name' => 'Create Vendor Credit (without line items support i-e Account Based Expense Line)',
                'slug' => 'create_vendor_credit_account_based',
                'status' => 1,
                'channel_id' => 16,
            ],

            [
                'name' => 'Create Class',
                'slug' => 'create_account',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Update Class',
                'slug' => 'update_class',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create Deposit',
                'slug' => 'create_deposit',
                'status' => 1,
                'channel_id' => 16,
            ],
            [
                'name' => 'Create a Term',
                'slug' => 'create_term',
                'status' => 1,
                'channel_id' => 16,
            ],


             // slack events starts here
                [
                   'name' => 'Send Channel Message',
                   'slug' => 'send_channel_message',
                   'status' => 1,
                   'channel_id' => 1,
               ],[
                   'name' => 'Send Direct Message',
                   'slug' => 'send_direct_message',
                   'status' => 1,
                   'channel_id' => 1,
               ],
               [
                   'name' => 'Set Channel Topic',
                   'slug' => 'set_channel_topic',
                   'status' => 1,
                   'channel_id' => 1,
               ],
               [
                   'name' => 'Add Reminder',
                   'slug' => 'add_reminder',
                   'status' => 1,
                   'channel_id' => 1,
               ],
               [
                   'name' => 'Invite User to Slack',
                   'slug' => 'invite_user_to_slack',
                   'status' => 1,
                   'channel_id' => 1,
               ]


        ];
        \App\Entities\ChannelEvent::insert($data);
    }
}

<div style="display: none;" class="modal fade" id="app-integration-modal" role="dialog">
    <div class="modal-dialog">
    <input type="hidden" id="webhook_event_id" value="0">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title">Channels</h3>
            </div>
            <div class="modal-body">
                <div class="channel-main-div">
                    {{--<div class="row top-header-area">--}}
                        {{--<div class="form-group col-md-4 search-div">--}}
                            {{--<i class="fa fa-search"></i>--}}
                            {{--<input type="text" class="form-control" placeholder="Search app...">--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-4 popular-dropdwon-div">--}}
                            {{--<div class="">--}}
                                {{--<select id="inputPapular-1" class="form-control">--}}
                                    {{--<option selected>popular</option>--}}
                                    {{--<option>...</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-4 popular-dropdwon-div">--}}
                            {{--<div class="">--}}
                                {{--<select id="inputPapular-2" class="form-control">--}}
                                    {{--<option selected>popular</option>--}}
                                    {{--<option>...</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row scrollable-Div">

                           @foreach($channels as $channel)

                            <span class="col-md-3 col-sm-6 col-12 scrollable-app-list">
                                <div class="app-integration">
                                    <a data-channel_id="{{ $channel->id }}" href="javascript:" class="app-integration-links">

                                            {{--icon summary area--}}
                                        <div class="parent-shall">
                                            <div class="service-icon-shell">
                                                    <img alt="{{ $channel->name }} logo" aria-hidden="false" class=""
                                                         src="{{ $channel->icon_path }}"
                                                    >
                                            </div>
                                        </div>

                                            {{--icon description area--}}



                                         <div class="app-icon-details">
                                                <h3 class="app-integration-name">{{ ucwords(str_replace('_',' ',$channel->name)) }}</h3>
                                            </div>
                                    </a>
                                </div>
                            </span>
                        @endforeach
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-offline-settings">Save</button>
            </div>
        </div>

    </div>
</div>
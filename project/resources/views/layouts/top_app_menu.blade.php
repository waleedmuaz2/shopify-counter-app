<script type="text/javascript">
initShopify : {
    <?php if (!empty(getenv('DEV_ALLOW_NO_EMBEDDED_FRAME'))) : ?>
        if (window === window.parent) break initShopify;
    <?php endif ?>

    <?php $shop_session = session('shop'); if ($shop_session)  $domain = $shop_session['myshopify_domain']; ?>
    ShopifyApp.init({
        apiKey: '<?php echo env('SHOPIFY_APIKEY'); ?>',
        shopOrigin: '<?php echo 'https://' . ($domain??''); ?>'

    });

    {{--ShopifyApp.ready(function () {--}}
    {{--    ShopifyApp.Bar.initialize({--}}
    {{--        buttons: {--}}
    {{--            secondary: [--}}
    {{--                {--}}
    {{--                    label: "mainapp page", href: '{{ route('main_app_page') }}', target: "app"}--}}
    {{--                ]--}}
    {{--        }--}}
    {{--    });--}}
    {{--    @if($response_message = session('message'))--}}
    {{--    ShopifyApp.flashNotice('{{ $response_message }}');--}}
    {{--     @endif--}}

    {{--     @if ($errors->any())--}}

    {{--     @foreach ($errors->all() as $error)--}}
    {{--     ShopifyApp.flashError('{{ $error }}');--}}
    {{--    @endforeach--}}

    {{--    @endif--}}
    {{--});--}}
}
</script>
<!DOCTYPE html>
<html lang="en">
<head >
    @include('layouts.head')
    @yield('head')
</head>

<body class="production">
{{--page loading gif html--}}
<div class="overlay">
    <div id="loading-img"></div>
</div>
@include('partials.upgrade_plan_alert')
{{--main page content section--}}
@yield('content')

</body>
@include('layouts.footer_scripts')


@yield('last_scripts')

@if(empty(getenv('DEV_DISABLE_TAWK_TO')))
    <!--Start of Tawk.to Script-->
{{--    <script type="text/javascript">--}}
{{--        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
{{--        (function(){--}}
{{--            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
{{--            s1.async=true;--}}
{{--            s1.src='https://embed.tawk.to/5f44cabdcc6a6a5947ae8eef/default';--}}
{{--            s1.charset='UTF-8';--}}
{{--            s1.setAttribute('crossorigin','*');--}}
{{--            s0.parentNode.insertBefore(s1,s0);--}}
{{--        })();--}}
{{--    </script>--}}
    <!--End of Tawk.to Script-->

@endif
<script type="text/javascript">
//this is a quick fix to avoid sessions conflicts in more than one stores open in same browser.Need to update it later
        setTimeout(function () {
                if(!ShopifyApp._isReady){
                    window.location.href = "{{ route('un_verrified_shopify_request',["message"=>"We're aware of this issue.If you're using this app in multiple stores then open each store in separate browser."]) }}";
                }
            },7000)

</script>

</html>
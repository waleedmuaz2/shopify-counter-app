@extends('layouts.master')
@section('content')
    <title>{{ env('APP_NAME',$title."_") }}</title>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
    <div>
        <button onclick="return   window.history.back();" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
            {{__('msg.back')}}</button>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table id="table" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>Sr #</th>
                    <th>{{__('msg.vistor_name')}}</th>
                    <th>{{__('msg.counter')}}</th>
                    <th>{{__('msg.last_view')}}</th>
                </tr>
                </thead>

                <tbody>
                @if($data)
                    @foreach($data as $mydata)
                        <tr>
                            <td>{{ $mydata['srNumber'] }}</td>
                            <td>{{ $mydata['vistor_name'] }}</td>
                            <td>{{ $mydata['counter'] }}</td>
                            <td>{{ $mydata['created_at'] }}</td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    @section('last_scripts')
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.print.min.js"></script>
        <script>
            $(document).ready(function() {

                $('#table').DataTable({
                    dom: 'lBfrtip',
                    destroy: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    buttons: [
                        {
                            extend: 'copy',
                            text: '{{__('msg.copy')}}',
                        },
                        {
                            extend: 'print',
                            text: '{{__('msg.Print')}}',
                        },{
                            extend: 'csv',
                            text: '{{__('msg.csv')}}',
                        }
                    ]
                } );
                $('#vistor').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    buttons: [
                        {
                            extend: 'copy',
                            text: '{{__('msg.copy')}}',
                        },
                        {
                            extend: 'print',
                            text: '{{__('msg.Print')}}',
                        },{
                            extend: 'csv',
                            text: '{{__('msg.csv')}}',
                        }
                    ]
                });


            } );
        </script>

    @endsection

<outbound_order>
    @foreach($array as $key=>$data)
        @if(!is_array($data))
            <{{$key}}>{!! $data !!}</{{$key}}>
        @else
            <{{$key}}>
            @foreach($data as $keys => $values)
                @if(!is_array($values))
                    <{{$keys}}>{!! $values !!}</{{$keys}}>
                @else
                    @if(!is_numeric($keys))
                        <{{$keys}}>
                    @endif
                    @foreach($values as $keyss => $valuess)
                        @if(!is_array($valuess))
                            <{{$keyss}}>{!! $valuess !!}</{{$keyss}}>
                        @else
                            @if(!is_numeric($keyss))
                            <{{$keyss}}>
                            @endif
                            @foreach($valuess as $keysss=> $valuesss)
                                @if(!is_array($valuesss))
                                    <{{$keysss}}>{!! $valuesss !!}</{{$keysss}}>
                                @endif
                            @endforeach
                            @if(!is_numeric($keyss))
                                </{{$keyss}}>
                            @endif
                        @endif
                    @endforeach
                    @if(!is_numeric($keys))
                        </{{$keys}}>
                        @endif
                @endif
            @endforeach
            </{{$key}}>
        @endif
    @endforeach
</outbound_order>
<?php  
	return
	[
		'blog_id' => 'ブログID',
		'post_id' => '投稿ID',
		'post_title' => '記事のタイトル',
		'vistor_name' => 'ユーザー名',
		'email' => 'Eメール',
		'total_spent' => '総支出',
		'total_guest_user' => 'ゲストユーザーの合計',
		'total_register_user' => '登録ユーザー総数',
		'total_visitor'=>'総訪問者',
		'sr'=>'順番',
        'last_view' =>'最後閲覧日時',
        'total_user'=>"トータル",
        "register_user"=>"会員ユーザー",
        "gu_user"=>"未登録ユーザー",
        "dashboard_details"=>"ダッシュボード",
        'vistor_list'=>"訪問者リスト",
        'settings'=>"設定",
        'listing_page'=>'一覧ページ',
        'detail_page'=>"ブログ内容ページ",
        'blog_name'=>"記事",
        'counter'=>'カウンター',
        'back'=>'戻る',
        'copy'=>'コピー',
        'Print'=>'印刷',
        'csv' =>'Csv',
        'upload'=>'アップロード'
    ];
?>
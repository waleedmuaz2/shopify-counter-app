<?php  
	return
	[
		'blog_id' => 'Blog ID',
		'post_id' => 'Post ID',
		'post_title' => 'Post Title',
		'vistor_name' => 'Visitor Name',
		'email' => 'Email',
		'total_spent' => 'Total Spent',
		'total_guest_user' => 'Total Guest User',
		'total_register_user' => 'Total Register User',
		'total_visitor'=>'Total Visitor',
		'sr'=>'Sr',
        'last_view' =>'Last View',
        'total_user'=>"Total User",
        "register_user"=>"Register User",
        "gu_user"=>"Guest User",
        "dashboard_details"=>"Dashboard Detail",
        'vistor_list'=>"Vistor List",
        'settings'=>"Settings",
        'listing_page'=>'Listing Page',
        'detail_page'=>"Detail Page",
        'blog_name'=>"Blog Name",
        'counter'=>'Counter',
        'back'=>'Back',
        'copy'=>'Copy',
        'Print'=>'Print',
        'csv'=> 'Csv',
        'upload'=>'Upload'
    ];
?>